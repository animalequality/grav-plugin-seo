<?php
namespace Grav\Plugin;

use Grav\Common\Plugin;
use RocketTheme\Toolbox\Event\Event;
use Grav\Common\Data\Blueprints;

/**
 * Class AnimalEqualitySEOPlugin
 * @package Grav\Plugin
 */
class AnimalEqualitySEOPlugin extends Plugin
{
    private $page = null;
    private $pageConfig = null;
    private $defaultTitle = '';

    /**
     * Generate a suitable string for meta description from page content
     *
     * @param [string] $pageContent
     *
     * @return string
     */
    private function pageContentToDescription($pageContent)
    {
        $description = strip_tags($pageContent);

        if (strlen($description) > 300) {
            $description = substr($description, 0, 300) . '...';
        }

        return $description;
    }

    /**
     * Retrieve URL of the given image. If it is a local image also width and height are returned 
     *
     * @param [string] $imagePath
     *
     * @return array
     */
    private function getImageDetails($imagePath, $image = null)
    {
        $imageDetails = [];

        if (substr($imagePath, 0, 4) === "http") {
            $imageDetails['url'] = $imagePath;
        } else {
            if (!$image) {
                $imageParts = pathinfo($imagePath);
                if (is_array($imageParts) && array_key_exists('basename', $imageParts) && array_key_exists('dirname', $imageParts)) {
                    $imageBasename = $imageParts['basename'];
                    $imagePage = $imageParts['dirname'] === '.' ? $this->page : $this->page->find($imageParts['dirname']);

                    if ($imagePage && array_key_exists(urldecode($imageBasename), $imagePage->media()->images())) {
                        $image = $imagePage->media()->images()[urldecode($imageBasename)];
                    }
                }
            }
            if ($image) {
                $imageSize = getimagesize($image->path());
                $imageDetails['url'] = $this->grav['uri']->base() . $image->url();
                if (is_array($imageSize)) {
                    $imageDetails['width'] = $imageSize[0];
                    $imageDetails['height'] = $imageSize[1];
                }
            }
        }

        return $imageDetails;
    }

    /**
     * Replaces the HTML title content in the given string with the proper SEO title
     * 
     * @param [string] $output
     *
     * @return string
     */
    private function updateOutputTitle($output)
    {
        $updatedOutput = preg_replace(
            '/(<title>)(.*?)(<\/title>)/i',
            '$1' . (($this->pageConfig->googleTitle ?? $this->defaultTitle) . ($this->pageConfig['appendSitename'] ? ' | ' . $this->config->site['title'] : '')) . '$3',
            $output
        );
        
        return $updatedOutput;
    }

    /**
     * Apply neccessary changes to a value which should be used as a meta tag value
     * 
     * @param [string] $value
     * 
     * @return string
     */
    private function processMetaValue($value)
    {
        return htmlspecialchars($value);
    }

    /**
     * Update the metadata object with the proper SEO data
     *
     * @param $metadata
     * @param [string] $defaultDescription
     *
     * @return void
     */
    private function getMetadata($metadata, $defaultDescription)
    {
        // Google Meta
        $googleTitle = $this->processMetaValue($this->pageConfig->googleTitle ?? $this->defaultTitle) . ($this->pageConfig['appendSitename'] ? ' | ' . $this->config->site['title'] : '');
        $metadata['title']['name'] = 'title';
        $metadata['title']['content'] = $googleTitle;

        $googleDescription = $this->processMetaValue($this->pageConfig->googleDescription ?? $defaultDescription);
        $metadata['description']['name'] = 'description';
        $metadata['description']['content'] = $googleDescription;

        // Facebook Meta
        $facebookTitle = $this->processMetaValue($this->pageConfig->facebookTitle ?? $this->defaultTitle) . ($this->pageConfig['appendSitename'] ? ' | ' . $this->config->site['title'] : '');
        $metadata['og:title']['property'] = 'og:title';
        $metadata['og:title']['content'] = $facebookTitle;

        $facebookDescription = $this->processMetaValue($this->pageConfig->facebookDescription ?? $defaultDescription);
        $metadata['og:description']['property'] = 'og:description';
        $metadata['og:description']['content'] = $facebookDescription;

        $metadata['og:type']['property'] = 'og:type';
        $metadata['og:type']['content'] = 'website';

        $metadata['og:url']['property'] = 'og:url';
        $metadata['og:url']['content'] = $this->page->canonical(true);

        $metadata['og:site_name']['property'] = 'og:site_name';
        $metadata['og:site_name']['content'] = $this->config->site['title'];

        if ($this->pageConfig->facebookAppId) {
            $metadata['fb:app_id']['property'] = 'fb:app_id';
            $metadata['fb:app_id']['content'] = $this->pageConfig->facebookAppId;
        }

        $facebookImageDetails = $this->getImageDetails(
            $this->pageConfig['facebookImage'],
            !$this->pageConfig['facebookImage'] && $this->page->media()->count() > 0 ? $this->page->media()->current() : null
        );

        if (array_key_exists('url', $facebookImageDetails)) {
            $metadata['og:image']['property'] = 'og:image';
            $metadata['og:image']['content'] = $facebookImageDetails['url'];
        }

        if (array_key_exists('width', $facebookImageDetails) && array_key_exists('height', $facebookImageDetails)) {
            $metadata['og:image:width']['property'] = 'og:image:width';
            $metadata['og:image:width']['content'] = $facebookImageDetails['width'];

            $metadata['og:image:height']['property'] = 'og:image:height';
            $metadata['og:image:height']['content'] = $facebookImageDetails['height'];
        }

        // Twitter Meta
        $metadata['twitter:card']['name'] = 'twitter:card';
        $metadata['twitter:card']['content'] = $this->pageConfig['twitterCardType'];

        $twitterTitle = $this->processMetaValue($this->pageConfig->twitterTitle ?? $this->defaultTitle) . ($this->pageConfig['appendSitename'] ? ' | ' . $this->config->site['title'] : '');
        $metadata['twitter:title']['name'] = 'twitter:title';
        $metadata['twitter:title']['content'] = $twitterTitle;
        
        $twitterDescription = $this->processMetaValue($this->pageConfig->twitterDescription ?? $defaultDescription);
        $metadata['twitter:description']['name'] = 'twitter:description';
        $metadata['twitter:description']['content'] = $twitterDescription;

        $metadata['twitter:url']['name'] = 'twitter:url';
        $metadata['twitter:url']['content'] = $this->grav['page']->canonical(true);
        
        if ($this->pageConfig->twitterId) {
            $metadata['twitter:site']['name'] = 'twitter:site';
            $metadata['twitter:site']['content'] = $this->pageConfig->twitterId;
        }
        
        $twitterImageDetails = $this->getImageDetails(
            $this->pageConfig['twitterImage'],
            !$this->pageConfig['twitterImage'] && $this->page->media()->count() > 0 ? $this->page->media()->current() : null
        );

        if (array_key_exists('url', $twitterImageDetails)) {
            $metadata['twitter:image']['name'] = 'twitter:image';
            $metadata['twitter:image']['content'] = $twitterImageDetails['url'];
        }

        return $metadata;
    }

    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents()
    {
        return [
            'onPluginsInitialized' => [
                ['onPluginsInitialized', 0]
            ]
        ];
    }

    /**
     * Initialize the plugin
     */
    public function onPluginsInitialized()
    {
        if ($this->isAdmin()) {
            $this->enable([
                'onBlueprintCreated' => ['onBlueprintCreated', 0]
            ]);
        } else {
            $this->enable([
                'onPageInitialized' => ['onPageInitialized', 0],
                'onOutputGenerated' => ['onOutputGenerated', 0]
            ]);
        }
    }

    /**
     * Extend page blueprints to include a SEO tab where the SEO information can be set
     *
     * @param Event $event
     *
     * @return void
     */
    public function onBlueprintCreated(Event $event)
    {
        if (strpos($event['type'], 'modular/') === false) {
            $blueprint = $event['blueprint'];
            if ($blueprint->get('form/fields/tabs', null, '/')) {
                $blueprints = new Blueprints(__DIR__ . '/blueprints/');
                $extends = $blueprints->get($this->name);
                $blueprint->extend($extends, true);
            }
        }
    }

    public function onPageInitialized(Event $event)
    {
        $this->page = $event['page'];
        $this->pageConfig = $this->mergeConfig($this->page);
        $this->defaultTitle = strip_tags($this->page->header()->title);

        $this->page->metadata($this->getMetadata(
            $this->page->metadata(null),
            $this->pageContentToDescription($this->page->content())
        ));
    }

    /**
     * Update the HTML title before outputting the content. This approach is used because
     * changing page->header()->title destroys the independency of HTML and page title
     *
     * @return void
     */
    public function onOutputGenerated()
    {
        $this->grav->output = $this->updateOutputTitle($this->grav->output);
    }
}
